# Bibliography for Assignment 1A F21_Proj_01A_Bellegarde_Thomas

Functions related to races, classes and alignments are based on the video, "Unity 5 UI Tutorial - Dropdown List"
by Sloan Kelly. Here is the link to that video: https://www.youtube.com/watch?v=Q4NYCSIOamY.

Input field functions below are influenced by the video: "How to display text from an input field
using C# Unity by Jimmy Vegas". Here is the link to that video: https://www.youtube.com/watch?v=2liZtyMhIQQ

The code snippet that finds the highest three dice rolls from the dice roll array is taken from an 
article on geeksforgeeks.com. Here is the ink:https://www.geeksforgeeks.org/find-the-largest-three-elements-in-an-array.

The function Json() that saves and displays the Json file is based on the video "What is JSON? (Unity Tutorial for Beginners)"
from CodeMonkey. It can be found at: https://www.youtube.com/watch?v=4oRVMCRCvN0.

The code for the function Exit(), which quits the game on a button click, was taken from GameDevBeginner.com. Here is the
link to that article. https://gamedevbeginner.com/how-to-quit-the-game-in-unity/

The art for the scene background is called FelisChaus_ParchmentBackground.jpg. It was downloaded from opengameart.com and 
can be found here: https://opengameart.org/content/parchment-background. 

The art for buttons is from a file called buttonStock1d.png. It was downloaded from opengameart.com and can be found here:
https://opengameart.org/content/ui-button.

.gitignore file used in this project taken from github.com. Here is the link to that file:
https://github.com/github/gitignore/blob/master/Unity.gitignore